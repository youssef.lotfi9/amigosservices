package org.notification.service;

import lombok.AllArgsConstructor;
import org.clients.notification.NotificationRequest;
import org.notification.model.Notification;
import org.notification.repository.NotificationRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class NotificationService {

    private final NotificationRepository notificationRepository;

    public void send(NotificationRequest notificationRequest) {

        notificationRepository.save(
                Notification.builder()
                        .message(notificationRequest.message())
                        .toCustomerEmail(notificationRequest.toCustomerEmail())
                        .toCustomerId(notificationRequest.toCustomerId())
                        .message("hard Coded message")
                        .sender("lotfey")
                        .sentAt(LocalDateTime.now())
                        .build());
    }

}
